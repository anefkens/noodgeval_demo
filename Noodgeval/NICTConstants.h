//
//  NICTConstants.h
//  Noodgeval
//
//  Created by Arnold Nefkens on 13-08-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#define NICTGreen [UIColor colorWithRed:53.0f/255.0f green:108.0f/255.0f blue:88.0f/255.0f alpha:1]
#define NICTGreenDark [UIColor colorWithRed:25.0f/255.0f green:55.0f/255.0f blue:32.0f/255.0f alpha:1.0f]
#define NICTGreenLight [UIColor colorWithRed:45.0f/255.0f green:90.0f/255.0f blue:70.0f/255.0f alpha:1.0f]
#define NICTOrange [UIColor colorWithRed:255.0f/255.0f green:127.0f/255.0f blue:0.0f/255.0f alpha:1.0f]


