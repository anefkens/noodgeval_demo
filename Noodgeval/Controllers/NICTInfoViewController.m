//
//  NICTInfoViewController.m
//  Noodgeval
//
//  Created by Arnold Nefkens on 24-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import "NICTInfoViewController.h"

@interface NICTInfoViewController ()

@property (nonatomic, strong) UIView *textView;

@end



@implementation NICTInfoViewController


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 320, 480)];
//    UIView *contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    contentView.backgroundColor = [UIColor darkGrayColor];
       
    
    self.textView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 392)];
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.textView.layer.shadowOffset = CGSizeMake(3, 3);
    self.textView.layer.shadowOpacity = 1;
    
    
    UITextView *textViewText = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, 290, 380)];
    textViewText.textColor = [UIColor darkGrayColor];
    textViewText.font = [UIFont systemFontOfSize:16];
    textViewText.text = NSLocalizedString(@"description", @"uitleg over app, en werking.");
    textViewText.editable = NO;
    [self.textView addSubview:textViewText];
    
    
    [contentView addSubview:self.textView];
    
    
    self.view = contentView;
    


}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
   self.navigationController.navigationBar.tintColor = NICTGreen;
    
    self.navigationItem.title = NSLocalizedString(@"InfoTitle", @"Info View Title");
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"Back Button") style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(dismissModalViewControllerAnimated:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"resetPrefs", @"reset prefs file") style:UIBarButtonItemStylePlain target:self action:@selector(resetData:)];
    
    
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.textView.center = self.view.center;
    
}




- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)resetData:(id)sender {
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setBool:NO forKey:@"person1Set"];
    [prefs setBool:NO forKey:@"person2Set"];
    [prefs setValue:@"" forKey:@"person1Name"];
    [prefs setValue:@"" forKey:@"person2Name"];
    
    [prefs synchronize];
    
}



@end
