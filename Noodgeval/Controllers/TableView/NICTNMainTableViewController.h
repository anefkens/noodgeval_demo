//
//  NICTNMainTableViewController.h
//  Noodgeval
//
//  Created by Arnold Nefkens on 12-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NICTInfoViewController.h"



@interface NICTNMainTableViewController : UITableViewController
<ABPeoplePickerNavigationControllerDelegate, ABPersonViewControllerDelegate, UITextViewDelegate>

@property (nonatomic, strong) UIImageView *ownerImage;
@property (nonatomic, strong) UILabel *ownerName;
@property (nonatomic, strong) UILabel *ownerBirthdate;
@property (nonatomic, strong) UITextView *medicalText;
@property (nonatomic, strong) UILabel *defaultMedical;
@property (nonatomic, strong) UIBarButtonItem *rightItem;
@property (nonatomic, strong) NSIndexPath *cellSelectedIndexPath;
@property (nonatomic, strong) NSUserDefaults *prefs;

@property (nonatomic, assign) BOOL pickOwner;
@property (nonatomic, assign) BOOL pickPerson1;
@property (nonatomic, assign) BOOL pickPerson2;


-(void)infoPressed:(id)sender;

@end
