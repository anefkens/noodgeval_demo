//
//  NICTNMainTableViewController.m
//  Noodgeval
//
//  Created by Arnold Nefkens on 12-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import "NICTNMainTableViewController.h"
#import "MainSelectionCustomCell.h"
#import "NICTInfoViewController.h"


@implementation NICTNMainTableViewController

@synthesize ownerName, ownerImage, ownerBirthdate;
@synthesize pickOwner, pickPerson1, pickPerson2, medicalText, defaultMedical, rightItem, prefs, cellSelectedIndexPath;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    self.prefs = [NSUserDefaults standardUserDefaults];
    
    self.pickOwner = NO;
    self.pickPerson1 = NO;
    self.pickPerson2 = NO;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 170)];
    headerView.backgroundColor = [UIColor clearColor];
    
    
    UILabel *ownerPrefix = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 280, 20)];
    ownerPrefix.text = NSLocalizedString(@"Owner's Name",@"Owner's Name");
    ownerPrefix.backgroundColor = [UIColor clearColor];
    ownerPrefix.font = [UIFont boldSystemFontOfSize:14];
    ownerPrefix.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:ownerPrefix];
    
    self.ownerImage = [[UIImageView alloc] initWithFrame:CGRectMake(200, 45, 78, 100)];
    self.ownerImage.clipsToBounds = NO;
    self.ownerImage.layer.shadowOffset = CGSizeMake(2, 2);
    self.ownerImage.layer.shadowOpacity = 1;
    self.ownerImage.layer.shadowRadius = 1.0;
    
    
    [headerView addSubview:self.ownerImage];
    
    self.ownerName = [[UILabel alloc] initWithFrame:CGRectMake(10, 45, 180, 35)];
    self.ownerName.backgroundColor = [UIColor clearColor];
    self.ownerName.font = [UIFont boldSystemFontOfSize:12];
    self.ownerName.text = [self.prefs valueForKey:@"OwnerName"];
    [headerView addSubview:self.ownerName];
    
    self.ownerBirthdate = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, 180, 35)];
    self.ownerBirthdate.backgroundColor = [UIColor clearColor];
    self.ownerBirthdate.font = [UIFont boldSystemFontOfSize:12];
    self.ownerBirthdate.text = [self.prefs valueForKey:@"OwnerBirthDate"];
    [headerView addSubview:self.ownerBirthdate];
    
    if ([self.prefs boolForKey:@"ownerSet"]) {
        self.ownerName.text = [self.prefs valueForKey:@"ownerName"];
        self.ownerBirthdate.text = [self.prefs valueForKey:@"ownerBirthdate"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
        UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
        self.ownerImage.image = img;
    }
    
    self.tableView.tableHeaderView = headerView;
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
    footerView.backgroundColor = [UIColor clearColor];
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoNICT"]];
    logo.frame = CGRectMake(13, 10, 290, 110);
    logo.clipsToBounds = NO;
    logo.layer.shadowColor = [UIColor blackColor].CGColor;
    logo.layer.shadowOffset = CGSizeMake(3, 3);
    logo.layer.shadowOpacity = 1;

    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	infoButton.frame = CGRectMake(265, 130, 56, 56);
	infoButton.backgroundColor = [UIColor clearColor];
	[infoButton addTarget:self action:@selector(infoPressed:) 
		 forControlEvents:UIControlEventTouchUpInside];    
	[footerView addSubview:infoButton];
    
    [footerView addSubview:logo];
    
    
    self.tableView.tableFooterView = footerView;

    self.tableView.backgroundView = nil;
    self.tableView.opaque = NO;
    
    self.medicalText = [[UITextView alloc] initWithFrame:CGRectMake(5, 5, 290, 150)];
    self.medicalText.delegate = self;
    self.medicalText.keyboardType = UIKeyboardTypeDefault;
    self.medicalText.font = [UIFont systemFontOfSize:14];
    self.medicalText.autocorrectionType = UITextAutocorrectionTypeNo;
    if ([self.prefs boolForKey:@"medicalInfoSet"]) {
        self.medicalText.text = [self.prefs valueForKey:@"medicalInfo"];
    }
    self.defaultMedical = [[UILabel alloc] initWithFrame:CGRectMake(12, 6, 290, 150)];
    self.defaultMedical.text = NSLocalizedString(@"TypMedicalInfo", @"Voer hier medische info in.");
    self.defaultMedical.backgroundColor = [UIColor clearColor];
    self.defaultMedical.font = [UIFont systemFontOfSize:17];
    self.defaultMedical.numberOfLines = 0;
    self.defaultMedical.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    //Set the colors based on the OS
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        ownerPrefix.textColor = NICTGreenDark;
        self.ownerName.textColor = [UIColor darkGrayColor];
        self.ownerImage.backgroundColor = [UIColor lightGrayColor];
        self.ownerImage.layer.shadowColor = [UIColor blackColor].CGColor;
        self.ownerBirthdate.textColor = [UIColor darkGrayColor];
        self.medicalText.textColor = [UIColor whiteColor];
        self.medicalText.backgroundColor = NICTGreen;
        self.defaultMedical.textColor = [UIColor lightGrayColor];
//        self.tableView.backgroundColor = [UIColor darkGrayColor];
    } else {
        ownerPrefix.textColor = [UIColor whiteColor];
        self.ownerName.textColor = [UIColor whiteColor];
        self.ownerImage.backgroundColor = [UIColor lightGrayColor];
        self.ownerImage.layer.shadowColor = [UIColor blackColor].CGColor;
        self.ownerBirthdate.textColor = [UIColor whiteColor];
        self.medicalText.textColor = [UIColor whiteColor];
        self.medicalText.backgroundColor = NICTGreen;
        self.defaultMedical.textColor = [UIColor lightGrayColor];
        self.tableView.backgroundColor = [UIColor darkGrayColor];
    }
    
    
    
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MainSelectionCustomCell";
        //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    MainSelectionCustomCell *cell = (MainSelectionCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MainSelectionCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0: {
            
            cell.cellTitle.text = NSLocalizedString(@"PickOwner", @"Kies uw eigen kaart uit");
            cell.cellSubTitle.text = NSLocalizedString(@"ExplainButton", @"Tap hier voor de keuze");
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            break;   
        }
        case 1: {
            if ([self.prefs boolForKey:@"person1Set"]) {
                cell.cellTitle.text = [self.prefs valueForKey:@"person1Name"];
                cell.cellSubTitle.text = NSLocalizedString(@"Call1stPerson", @"Bel eerste Persoon");
            } else {
                cell.cellTitle.text = NSLocalizedString(@"Pick1stPerson", @"Kies eerste Persoon");
                cell.cellSubTitle.text = NSLocalizedString(@"ExplainButton", @"Tap hier voor de keuze");
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            break;
        }
        case 2: {
            if ([self.prefs boolForKey:@"person2Set"]) {
                cell.cellTitle.text = [self.prefs valueForKey:@"person2Name"];
                cell.cellSubTitle.text = NSLocalizedString(@"Call2ndPerson", @"Bel tweede Persoon");
                cell.cellSubTitle.hidden = NO;
            } else {
                cell.cellTitle.text = NSLocalizedString(@"Pick2ndPerson", @"Kies tweede persoon");
                cell.cellSubTitle.text = NSLocalizedString(@"ExplainButton", @"Tap hier voor de keuze");
            }
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            break;
        }
        case 3:
            switch (indexPath.row) {
                case 0: {
                    [cell.contentView addSubview:self.medicalText];
                    [cell.contentView addSubview:self.defaultMedical];
                    if ([self.prefs boolForKey:@"medicalInfoSet"]) {
                        self.defaultMedical.hidden = YES;
                    }
                    
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    break;
                }

                default:
                    break;
            }
            break;

    }

    cell.backgroundColor = NICTGreen;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
        //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
	picker.peoplePickerDelegate = self;

    switch (indexPath.section) {
        case 0: {
            self.pickOwner = YES;
            [self presentViewController:picker animated:YES completion:nil];
            break;
        }
        case 1:{
            if ([self.prefs boolForKey:@"person1Set"]) {
                NSURL *phone = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [self.prefs valueForKey:@"phone1Number"]]];
                [[UIApplication sharedApplication] openURL:phone];
            } else {
                self.pickPerson1 = YES;
                [self presentViewController:picker animated:YES completion:nil];
            }
            break;
        }
        case 2: {
            if ([self.prefs boolForKey:@"person2Set"]) {
                NSURL *phone = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [self.prefs valueForKey:@"phone2Number"]]];
                [[UIApplication sharedApplication] openURL:phone];
            } else {
                self.pickPerson2 = YES;
                [self presentViewController:picker animated:YES completion:nil];
            }
            break;
        }
            
        default:
            break;
    }
    
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    sectionHeaderView.backgroundColor = [UIColor clearColor];
    UILabel *sectionHeaderText = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320, 20)];
    sectionHeaderText.backgroundColor = [UIColor clearColor];
    sectionHeaderText.font = [UIFont boldSystemFontOfSize:16];
    sectionHeaderText.textAlignment = NSTextAlignmentCenter;
    //Set the color of the header view
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        sectionHeaderText.textColor = NICTGreenDark;
    } else {
        sectionHeaderText.textColor = [UIColor whiteColor];
    }
    
    
    switch (section) {
        case 0: {
            sectionHeaderText.text = NSLocalizedString(@"ownerSelection", @"Kies de owner sectie");
            break;
        }
        case 1: {
            sectionHeaderText.text = NSLocalizedString(@"firstPersonSelection", @"Kies de eerste persoon sectie");
            break;
        }
        case 2: {
            sectionHeaderText.text = NSLocalizedString(@"secondPersonSelection", @"Kies de tweede persoon sectie");
            break;
        }
        case 3: {
            sectionHeaderText.text = NSLocalizedString(@"MedicalInformation", @"medische sectie");
            break;
        }
    }
    [sectionHeaderView addSubview:sectionHeaderText];

    
    return sectionHeaderView;

}

-(CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rowHeight = 50;
    if (indexPath.section == 3) {
        rowHeight = 160;
    }
    
    return rowHeight;
}


#pragma mark - Own Methods

-(void)infoPressed:(id)sender
{
    
    NICTInfoViewController *infoControll = [[NICTInfoViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:infoControll];
    navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self.navigationController presentViewController:navController animated:YES completion:nil];
    
}


#pragma mark - Adres Book Delegates

#pragma mark peoplePicker Methods


-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
	 shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    
    if (self.pickOwner) {
      	NSString *firstName = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty)); //String van voornaam
        NSString *lastName = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty)); //String van achternaam
        NSDate *geboorteDatum = (NSDate *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonBirthdayProperty)); // geboortedatum
        NSData *ownerPhotoDataSrc = (NSData *)CFBridgingRelease(ABPersonCopyImageData(person)); //Data van de foto als data opgeslagen
        self.ownerName.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            //NSDate *dateToString = (NSDate *)ABRecordCopyValue(person, kABPersonBirthdayProperty);
        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
        [inputFormatter setDateFormat:@"dd-MM-yyyy"];
        self.ownerBirthdate.text = [inputFormatter stringFromDate:geboorteDatum];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        if(ABPersonHasImageData(person)){
            UIImage *ownerPhotoData = [UIImage imageWithData:ownerPhotoDataSrc];
            NSData *imageData = UIImagePNGRepresentation(ownerPhotoData);
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
            [imageData writeToFile:savedImagePath atomically:NO];
        }else{
            UIImage *ownerPhotoData = [UIImage imageNamed:@"contact_image.gif"];
            NSData *imageData = UIImagePNGRepresentation(ownerPhotoData);
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
            [imageData writeToFile:savedImagePath atomically:NO];
        }
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
        self.ownerImage.image = [UIImage imageWithContentsOfFile:getImagePath];
        [self.prefs setValue:self.ownerName.text forKey:@"ownerName"];
        [self.prefs setValue:self.ownerBirthdate.text forKey:@"ownerBirthDate"];
        [self.prefs setBool:YES forKey:@"ownerSet"];
        [self.prefs synchronize];
        [self dismissViewControllerAnimated:YES completion:nil];
        self.pickOwner = NO;
        return NO;
    } else {
        NSString *firstName = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        if (self.pickPerson1) {
            [self.prefs setValue:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"person1Name"];
        } else if (self.pickPerson2 ) {
            [self.prefs setValue:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"person2Name"];
        }
    }
    return YES;
}


	//Method call out
-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
	 shouldContinueAfterSelectingPerson:(ABRecordRef)person
							   property:(ABPropertyID)property
							 identifier:(ABMultiValueIdentifier)indentifier
{
	
	ABPersonViewController *controller = [[ABPersonViewController alloc] init];
    controller.displayedPerson = person;
    controller.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonPhoneProperty]];
    controller.personViewDelegate = self;
    [peoplePicker pushViewController:controller animated:YES];
    
	return NO;
		//return YES; //make a call to number selected.... Or send a email depending on type.
}


- (BOOL)personViewController:(ABPersonViewController *)personViewController 
shouldPerformDefaultActionForPerson:(ABRecordRef)person 
                    property:(ABPropertyID)property
                  identifier:(ABMultiValueIdentifier)identifierForValue
{
    
    ABMutableMultiValueRef multi = ABRecordCopyValue(person, property);
    CFStringRef phone = ABMultiValueCopyValueAtIndex(multi, identifierForValue);
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *originalPhone = (__bridge NSString *)phone;
    NSMutableString *strippedPhone = [NSMutableString stringWithCapacity:originalPhone.length];
    NSScanner *scanner = [NSScanner scannerWithString:originalPhone];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"+-0123456789"];
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedPhone appendString:buffer];
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
	if (self.pickPerson1) {
        [self.prefs setValue:strippedPhone forKey:@"phone1Number"];
        [self.prefs setBool:YES forKey:@"person1Set"];
        self.pickPerson1 = NO;
    } else if (self.pickPerson2) {
        [self.prefs setValue:strippedPhone forKey:@"phone2Number"];
        [self.prefs setBool:YES forKey:@"person2Set"];
        self.pickPerson2 = NO;
    }
    [self.prefs synchronize];
    [self.tableView reloadData];
    CFRelease(phone);
	CFRelease(multi);
	
	ABPeoplePickerNavigationController *peoplePicker = (ABPeoplePickerNavigationController *)personViewController.navigationController;
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    return NO;
}


#pragma mark - UITextView Delegates

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editTextViewDone:)];
    [self.navigationItem setRightBarButtonItem:self.rightItem animated:YES];
    self.medicalText = textView;
    self.defaultMedical.hidden = YES;
    
}

-(void)editTextViewDone:(id)sender
{
    [self.medicalText resignFirstResponder];
    if ([self.medicalText.text isEqualToString:@""]) {
        self.defaultMedical.hidden = NO;
    }
    [self.prefs setValue:self.medicalText.text forKey:@"medicalInfo"];
    [self.prefs setBool:YES forKey:@"medicalInfoSet"];
    [self.prefs synchronize];
    [self.navigationItem setRightBarButtonItem:Nil animated:YES];
}

@end
