//
//  main.m
//  Noodgeval
//
//  Created by Arnold Nefkens on 12-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}
