//
//  NoodgevalAppDelegate.h
//  Noodgeval
//
//  Created by Arnold Nefkens on 12-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NICTNMainTableViewController.h"

@interface NoodgevalAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) UINavigationController *navControl;
@property (nonatomic, strong) NICTNMainTableViewController *nictMainTableViewControl;


@end
