//
//  MainSelectionCustomCell.h
//  Noodgeval
//
//  Created by Arnold Nefkens on 17-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainSelectionCustomCell : UITableViewCell
{
    UILabel *cellTitle;
    UILabel *cellSubTitle;
    
    
}

@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic, strong) UILabel *cellSubTitle;


@end
