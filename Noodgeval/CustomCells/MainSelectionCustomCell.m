//
//  MainSelectionCustomCell.m
//  Noodgeval
//
//  Created by Arnold Nefkens on 17-07-11.
//  Copyright 2011 Nefkens ICT. All rights reserved.
//

#import "MainSelectionCustomCell.h"

@implementation MainSelectionCustomCell
@synthesize cellTitle, cellSubTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        contentView.backgroundColor = [UIColor clearColor];
        self.cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
        cellTitle.textAlignment = NSTextAlignmentCenter;
        cellTitle.font = [UIFont boldSystemFontOfSize:18];
        
        cellTitle.backgroundColor = [UIColor clearColor];
        [contentView addSubview:cellTitle];

        self.cellSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,25, 320, 20)];
        cellSubTitle.textAlignment = NSTextAlignmentCenter;
        cellSubTitle.font = [UIFont systemFontOfSize:12];
        cellSubTitle.backgroundColor = [UIColor clearColor];
        
        [contentView addSubview:cellSubTitle];
        
        //Set the colors
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            cellTitle.textColor = [UIColor lightGrayColor];
            cellSubTitle.textColor = [UIColor lightGrayColor];
        } else {
            cellTitle.textColor = [UIColor lightGrayColor];
            cellSubTitle.textColor = [UIColor lightGrayColor];
        }
        
        
        [self.contentView addSubview:contentView];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
