//
//  Noodgeval_Tests.m
//  Noodgeval Tests
//
//  Created by Arnold Nefkens on 19-10-13.
//  Copyright (c) 2013 Nefkens ICT. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Noodgeval_Tests : XCTestCase

@end

@implementation Noodgeval_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTAssertTrue(YES, @"Demo Test Case"); //This will never fail!!!!
}

@end
